package com.practice.library.toast_util

import android.content.Context
import android.widget.Toast

public class ToastUtil {

    companion object {

        private const val TAG = "toast_tag"

        public fun showToast(context: Context, message: String) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }


    }
}